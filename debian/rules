#!/usr/bin/make -f
#-*- makefile -*-
# Rules file for Snort, developed by Javier Fernandez-Sanguino
# Made with the aid of dh_make, by Craig Small
# Sample debian/rules that uses debhelper. GNU copyright 1997 by Joey Hess.
# Some lines taken from debmake, by Christoph Lameter.
#

# based on the sample debian/rules that uses debhelper.
# This file was originally written by Joey Hess and Craig Small.
# As a special exception, when this file is copied by dh-make into a
# dh-make output file, you may use that output file without restriction.
# This special exception was added by Craig Small in version 0.37 of dh-make.

# Uncomment this to turn on verbose mode.
#export DH_VERBOSE=1

# Enable all hardening features as Snort is a network "listener" of sorts
export DEB_BUILD_MAINT_OPTIONS = hardening=+all
export CONFFLAGS= \
	--libdir=\$$\{prefix\}/lib/snort \
	--bindir=\$$\{exec_prefix\}/sbin \
	--mandir=\$$\{exec_prefix\}/share/man \
	--enable-gre \
	--enable-mpls \
	--enable-targetbased \
	--enable-ppm \
	--enable-active-response \
	--enable-normalizer \
	--enable-reload \
	--enable-react \
	--enable-flexresp3

%:
	dh $@ 

override_dh_install:
	dh_install
	! [ -e debian/snort/etc/default/snort.default ] || mv debian/snort/etc/default/snort.default debian/snort/etc/default/snort
	! [ -e debian/snort/etc/ppp/ip-up.d/snort.ip-up.d ] || mv debian/snort/etc/ppp/ip-up.d/snort.ip-up.d debian/snort/etc/ppp/ip-up.d/snort
	! [ -e debian/snort/etc/ppp/ip-down.d/snort.ip-down.d ] || mv debian/snort/etc/ppp/ip-down.d/snort.ip-down.d debian/snort/etc/ppp/ip-down.d/snort

override_dh_installdocs:
	dh_installdocs
	# Remove files in snort-doc which were present in older versions of snort
	rm -f debian/snort-doc/usr/share/doc/snort/README.PLUGINS  debian/snort-doc/usr/share/doc/snort/README.csv


override_dh_fixperms:
	dh_fixperms
	! [ -e debian/snort-common/etc/snort/attribute_table.dtd ] || chmod -x debian/snort-common/etc/snort/attribute_table.dtd
	! [ -e debian/snort/etc/ppp/ip-up.d/snort ] || chmod +x  debian/snort/etc/ppp/ip-up.d/snort
	! [ -e debian/snort/etc/ppp/ip-down.d/snort ] || chmod +x  debian/snort/etc/ppp/ip-down.d/snort

override_dh_auto_configure:
# Cannot use this as libdir is defined twice
#	dh_auto_configure -- $(CONFFLAGS)
	./configure --build=i686-linux-gnu --prefix=/usr --includedir=\$$\{prefix\}/include --mandir=\$$\{prefix\}/share/man --infodir=\$$\{prefix\}/share/info --sysconfdir=/etc --localstatedir=/var --disable-silent-rules --runstatedir=/run --disable-maintainer-mode --disable-dependency-tracking --disable-perfprofiling $(CONFFLAGS) 


override_dh_clean:
# Clean documents
	cd doc && [ ! -f Makefile ] || $(MAKE) distclean
	dh_clean


override_dh_auto_build-indep: doc/snort_manual.pdf

doc/snort_manual.pdf:
# Build the documentation
	$(MAKE) -C doc snort_manual.pdf 
# Only do this if we have the required binaries and files
# Note: I hate hardcoding paths here, but the buildds are stupid
#	@if [ -x /usr/bin/ps2pdf -a -x /usr/bin/pslatex -a \
#	     -x /usr/bin/dvips -a \
#	     -e /usr/share/texlive/texmf-dist/tex/latex/pslatex/pslatex.sty -a \
#	     -e /usr/share/texlive/texmf-dist/tex/latex/oberdiek/kvoptions.sty ] ; \
#	then \
#	    $(MAKE) -C doc snort_manual.pdf ; \
#	else \
#	    echo >&2 'WARNING: Not building documentation, environment not available!'; \
#	fi

override_dh_auto_test-indep:


update-rules:
	mv rules rules.old
	wget -q -O ../snortrules-snapshot-2_2.tar.gz http://www.snort.org/dl/rules/snortrules-snapshot-2_2.tar.gz
	tar -zxf ../snortrules-snapshot-2_2.tar.gz
	if [ -d rules ] ; then mv rules.old/Makefile* rules/; rm -rf rules.old; fi

# Update the Debconf templates for all packages using the common templates
update-templates: debian/snort.TEMPLATE.templates 
	cd debian && sh generate-templates.sh
	cd debian && debconf-updatepo

