Description: Add specific Debian documentation
 in the Snort manual packages. Including:
 - Setup of configuration files 
 - Logging and reporting as configured 
Author: Javier Fernández-Sanguino Peña <jfs@debian.org>
Origin: vendor
Last-Update: 2020-04-13


--- a/snort.8
+++ b/snort.8
@@ -951,6 +951,77 @@
 Please refer to manual for more details. Any other signal might cause the 
 daemon to close all opened files and exit.
 
+.SH CONFIGURATION
+In Debian, there are several ways in which Snort can be configured. The configuration
+file 
+.IR /etc/snort/snort.conf
+provides the configuration for the software itself. Users can customise this configuration.
+In systems which have multiple interfaces, it is possible to have a different Snort
+instance per network interface and adjust the specific configuration for one interface
+using 
+.IR /etc/snort/snort.INTERFACE.conf
+(where
+.B INTERFACE
+should be replaced by the interface name).
+
+There are additional configuration files in /etc/snort which modify Snort behaviour. These
+include: attribute_table.dtd, file_magic.conf, threshold.conf and unicode.map
+
+In addition, Debian provides a specific configuration file to manage the startup of Snort through the
+.IR /etc/snort/snort.debian.conf
+configuration file. This file is modified by the packaging system, using \fBdebconf\fR, and defines
+whether Snort is to be started up on system boot or manually, defines specific
+options for the Snort daemon when it is started and sets values to be used by the 
+.B snort-stat
+cron script (if enabled).
+
+Finally, the configuration file
+.IR /etc/default/snort 
+is used to define parameters which are applicable to the Snort startup (init.d)
+script. These include: daemon startup parameters, user and group Snort will run
+as, log directory and whether to run Snort when the interfaces to be monitored
+are not available.
+
+.SH LOGS AND ALERTS
+In Debian, the 
+.B Snort
+logs are available under /var/log/snort/ and includes
+.IR /var/log/snort/snort.log , 
+.IR /var/log/snort/snort.alert
+and
+.IR /var/log/snort/snort.alert.fast
+
+The first two of these logs are saved using the unified format 
+which can be read using the
+.B u2spewfoo
+tool.  For more information read /usr/share/doc/snort/README.unified2 which is
+provided by the snort-doc package. The log files in unified2 format can also be
+converted to other formats (currently only pcap is supported) using the 
+.BR u2boat
+tool. The last log file (snort.alert.fast) is a one line format that provides fast
+alerts. These alerts are read by the 
+.BR snort-stat
+and sent by email to a designed administrator if eneabled in the Debian package
+configuration.
+
+The location of the the log directory can be adjusted through the configuration of the 
+.B LOGDIR
+parameter in the 
+.IR /etc/default/snort 
+configuration file.
+
+.PP
+The log file 
+.IR /var/log/snort/snort.log 
+contains the packets logged, while the 
+.IR /var/log/snort/snort.alert
+contains only the alerts generated.
+
+In addition to this, all alerts are logged into syslog using LOG_AUTH and LOG_ALERT.
+
+The logging and alerting  mode can be modified by configuring the /etc/snort/snort.conf file.
+
+
 .SH HISTORY
 .B Snort
 has been freely available under the GPL license since 1998.
@@ -958,10 +1029,32 @@
 .B Snort
 returns a 0 on a successful exit, 1 if it exits on an error.
 .SH BUGS
-After consulting the BUGS file included with the source distribution, send bug
-reports to snort-devel@lists.snort.org
+After consulting the BUGS file included with the source distributon and available in Debian systems in
+/usr/share/doc/BUGS, as well as the Debian-specific bugs published in 
+https://bugs.debian.org/cgi-bin/pkgreport.cgi?package=snort please send bug reports to Debian using
+the 
+.B reportbug
+program. For more information about reporting bugs in Debian please read https://www.debian.org/Bugs/Reporting
+
+If you believe the bug lies with the upstream package, please send bug 
+reports directly to snort-devel@lists.snort.org
 .SH AUTHOR
-Martin Roesch <roesch@snort.org>
+The main author of Snort is Martin Roesch <roesch@snort.org>
+
+In addition, many people have contributed to Snort development. For a full list please read
+/usr/share/doc/snort/CREDITS.gz
+
+This Debian package was created by  Christian Hammers <ch@debian.org> (from
+1999 to 2001), Robert van der Meulen <rvdm@debian.org> (2001 to 2002), Sander
+Smeenk <ssmeenk@debian.org> (2002 to 2004) and Javier Fernández-Sanguino
+<jfs@debian.org> (2004 to 2020). It includes with contributions from many
+different Debian developers and users. All of them are credited in the Debian
+changelog file which can be found at /usr/share/doc/snort/changelog.Debian.gz
+and /usr/share/doc/snort/copyright
+
 .SH "SEE ALSO"
 .BR tcpdump (1),
-.BR pcap (3)
+.BR pcap (3),
+.BR u2boat (8),
+.BR u2spewfoo (8),
+.BR snort-stat (8)
