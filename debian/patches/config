Description: Debianize the upstream Snort configuration file
 Make the paths FHS compliant and absolute
 Add in additional rules shipped with the Debian package
 Include community rules
 Disable experimental preprocessors.
 Enable one output plugin (unified2) for packet logs
 Enable one output plugin (unified2) for alert logs
 Enable alert fast output plugin for alert logs to be used
 by the snort-stat script
 Add notes inline to Debian users
 Define max_gzip_mem as recommended in configuration guides
 Add Debian-specific comment to handle multiple interfaces
Author: Javier Fernández-Sanguino Peña <jfs@debian.org>
Origin: vendor
Last-Update: 2020-04-13

--- a/etc/snort.conf
+++ b/etc/snort.conf
@@ -38,14 +38,38 @@
 ###################################################
 
 ###################################################
+# Step #0: (Debian specific) Create a configuration 
+#          for a specific interface
+###################################################
+#
+# If you want to run Snort in Debian using different
+# instances each handling a different interface and
+# a different configuration you can copy this file to
+# /etc/snort/snort.$interface.conf (where '$interface' is the name of your
+# network interface) and adjust the valuse there.  
+#
+# The Debian init.d script is defined in such a way
+# that you can run multiple instances.
+
+###################################################
 # Step #1: Set the network variables.  For more information, see README.variables
 ###################################################
 
 # Setup the network addresses you are protecting
+#
+# Note to Debian users: this value is overriden when starting
+# up the Snort daemon through the init.d script by the
+# value of DEBIAN_SNORT_HOME_NET s defined in the
+# /etc/snort/snort.debian.conf configuration file
+#
 ipvar HOME_NET any
 
 # Set up the external network addresses. Leave as "any" in most situations
 ipvar EXTERNAL_NET any
+# If HOME_NET is defined as something other than "any", alternative, you can
+# use this definition if you do not want to detect attacks from your internal
+# IP addresses:
+#ipvar EXTERNAL_NET !$HOME_NET
 
 # List of DNS servers on your network 
 ipvar DNS_SERVERS $HOME_NET
@@ -101,17 +125,17 @@
 # Path to your rules files (this can be a relative path)
 # Note for Windows users:  You are advised to make this an absolute path,
 # such as:  c:\snort\rules
-var RULE_PATH ../rules
-var SO_RULE_PATH ../so_rules
-var PREPROC_RULE_PATH ../preproc_rules
+var RULE_PATH /etc/snort/rules
+var SO_RULE_PATH /etc/snort/so_rules
+var PREPROC_RULE_PATH /etc/snort/preproc_rules
 
 # If you are using reputation preprocessor set these
 # Currently there is a bug with relative paths, they are relative to where snort is
 # not relative to snort.conf like the above variables
 # This is completely inconsistent with how other vars work, BUG 89986
 # Set the absolute path appropriately
-var WHITE_LIST_PATH ../rules
-var BLACK_LIST_PATH ../rules
+var WHITE_LIST_PATH /etc/snort/rules
+var BLACK_LIST_PATH /etc/snort/rules
 
 ###################################################
 # Step #2: Configure the decoder.  For more information, see README.decode
@@ -244,13 +268,13 @@
 ###################################################
 
 # path to dynamic preprocessor libraries
-dynamicpreprocessor directory /usr/local/lib/snort_dynamicpreprocessor/
+dynamicpreprocessor directory /usr/lib/snort/snort_dynamicpreprocessor/
 
 # path to base preprocessor engine
-dynamicengine /usr/local/lib/snort_dynamicengine/libsf_engine.so
+dynamicengine /usr/lib/snort/snort_dynamicengine/libsf_engine.so
 
 # path to dynamic rules libraries
-dynamicdetection directory /usr/local/lib/snort_dynamicrules
+dynamicdetection directory /usr/lib/snort/snort_dynamicrules
 
 ###################################################
 # Step #5: Configure preprocessors
@@ -295,7 +319,7 @@
 # preprocessor perfmonitor: time 300 file /var/snort/snort.stats pktcnt 10000
 
 # HTTP normalization and anomaly detection.  For more information, see README.http_inspect
-preprocessor http_inspect: global iis_unicode_map unicode.map 1252 compress_depth 65535 decompress_depth 65535
+preprocessor http_inspect: global iis_unicode_map unicode.map 1252 compress_depth 65535 decompress_depth 65535 max_gzip_mem 104857600
 preprocessor http_inspect_server: server default \
     http_methods { GET POST PUT SEARCH MKCOL COPY MOVE LOCK UNLOCK NOTIFY POLL BCOPY BDELETE BMOVE LINK UNLINK OPTIONS HEAD DELETE TRACE TRACK CONNECT SOURCE SUBSCRIBE UNSUBSCRIBE PROPFIND PROPPATCH BPROPFIND BPROPPATCH RPC_CONNECT PROXY_SUCCESS BITS_POST CCM_POST SMS_POST RPC_IN_DATA RPC_OUT_DATA RPC_ECHO_DATA } \
     chunk_length 500000 \
@@ -503,13 +527,18 @@
    memcap 262144 \
    check_crc
 
+#
+# Note to Debian users: this is disabled since it is an experimental
+# preprocessor. If you want to use it you have to create the rules files
+# referenced below in the /etc/snort/rules directory
+#
 # Reputation preprocessor. For more information see README.reputation
-preprocessor reputation: \
-   memcap 500, \
-   priority whitelist, \
-   nested_ip inner, \
-   whitelist $WHITE_LIST_PATH/white_list.rules, \
-   blacklist $BLACK_LIST_PATH/black_list.rules 
+#preprocessor reputation: \
+#   memcap 500, \
+#   priority whitelist, \
+#   nested_ip inner, \
+#   whitelist $WHITE_LIST_PATH/white_list.rules, \
+#   blacklist $BLACK_LIST_PATH/black_list.rules
 
 ###################################################
 # Step #6: Configure output plugins
@@ -519,9 +548,11 @@
 # unified2 
 # Recommended for most installs
 # output unified2: filename merged.log, limit 128, nostamp, mpls_event_types, vlan_event_types
+output unified2: filename snort.log, limit 128, nostamp, mpls_event_types, vlan_event_types
 
 # Additional configuration for specific types of installs
 # output alert_unified2: filename snort.alert, limit 128, nostamp
+output alert_unified2: filename snort.alert, limit 128, nostamp
 # output log_unified2: filename snort.log, limit 128, nostamp 
 
 # syslog
@@ -530,6 +561,9 @@
 # pcap
 # output log_tcpdump: tcpdump.log
 
+# Fast alert logging for the daily cron script in Debian
+output alert_fast: snort.alert.fast
+
 # metadata reference data.  do not modify these lines
 include classification.config
 include reference.config
@@ -542,6 +576,10 @@
 # NOTE: All categories are enabled in this conf file
 ###################################################
 
+# Note to Debian users: The rules preinstalled in the system
+# can be *very* out of date. For more information please read
+# the /usr/share/doc/snort-rules-default/README.Debian file
+
 # site specific rules
 include $RULE_PATH/local.rules
 
@@ -649,6 +687,19 @@
 include $RULE_PATH/web-misc.rules
 include $RULE_PATH/web-php.rules
 include $RULE_PATH/x11.rules
+include $RULE_PATH/community-sql-injection.rules
+include $RULE_PATH/community-web-client.rules
+include $RULE_PATH/community-web-dos.rules
+include $RULE_PATH/community-web-iis.rules
+include $RULE_PATH/community-web-misc.rules
+include $RULE_PATH/community-web-php.rules
+include $RULE_PATH/community-sql-injection.rules
+include $RULE_PATH/community-web-client.rules
+include $RULE_PATH/community-web-dos.rules
+include $RULE_PATH/community-web-iis.rules
+include $RULE_PATH/community-web-misc.rules
+include $RULE_PATH/community-web-php.rules
+
 
 ###################################################
 # Step #8: Customize your preprocessor and decoder alerts
